au BufRead, BufNewFile *.c set filetype=c
setlocal noexpandtab
setlocal shiftwidth=8
setlocal tabstop=8
setlocal softtabstop=8
